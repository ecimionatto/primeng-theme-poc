import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-panel',
  templateUrl: './search-panel.component.html',
  styleUrls: ['./search-panel.component.css'],
})
export class SearchPanelComponent implements OnInit {
  first: String = '';
  second: String = '';
  third: String = '';

  isPanelVisible = false;

  selectedValues: string[] = ['val1', 'val2'];
  constructor() {}

  handlePanel(): void {
    this.isPanelVisible = !this.isPanelVisible;
  }
  handleSearch(): void {
    this.isPanelVisible = !this.isPanelVisible;
  }

  ngOnInit(): void {}
}
