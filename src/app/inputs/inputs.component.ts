import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.css'],
})
export class InputsComponent implements OnInit {
  first: string = 'test';
  last: string = 'test ';
  value: Date = new Date();
  selectedCityCode: string = '';

  cities: any = [
    { name: 'New York', code: 'NY' },
    { name: 'Rome', code: 'RM' },
    { name: 'London', code: 'LDN' },
    { name: 'Istanbul', code: 'IST' },
    { name: 'Paris', code: 'PRS' },
  ];

  constructor() {}

  ngOnInit(): void {}

  handleEvent(f: NgForm) {
    console.log('first:' + this.first + ' last:' + this.last); // { first: '', last: '' }
  }

  handleDate(f: NgForm) {
    console.log('date:' + this.value); // { first: '', last: '' }
  }
}
