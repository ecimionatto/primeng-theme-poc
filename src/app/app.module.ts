import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuModule } from 'primeng/menu';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TimelineComponent } from './timeline/timeline.component';
import { MatButtonModule } from '@angular/material/button';
import { TimelineModule } from 'primeng/timeline';
import { TableComponent } from './table/table.component';
import { TableModule } from 'primeng/table';
import { CheckboxModule } from 'primeng/checkbox';
import { ProgressBarModule } from 'primeng/progressbar';
import { ButtonsComponent } from './buttons/buttons.component';
import { ButtonModule } from 'primeng/button';
import { PanelModule } from 'primeng/panel';
import { InputMaskModule } from 'primeng/inputmask';
import { InputsComponent } from './inputs/inputs.component';
import { CalendarModule } from 'primeng/calendar';
import { VirtualScrollerModule } from 'primeng/virtualscroller';
import { InputTextModule } from 'primeng/inputtext';
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ScrollerComponent } from './scroller/scroller.component';
import { SelectButtonModule } from 'primeng/selectbutton';
import { SearchPanelComponent } from './search-panel/search-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    TimelineComponent,
    TableComponent,
    ButtonsComponent,
    InputsComponent,
    ScrollerComponent,
    SearchPanelComponent,
  ],
  imports: [
    VirtualScrollerModule,
    CalendarModule,
    FormsModule,
    ReactiveFormsModule,
    PanelModule,
    CheckboxModule,
    TimelineModule,
    MenuModule,
    MatButtonModule,
    BrowserModule,
    AppRoutingModule,
    TableModule,
    BrowserAnimationsModule,
    ProgressBarModule,
    ButtonModule,
    InputMaskModule,
    InputTextModule,
    SelectButtonModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class AppModule {}
