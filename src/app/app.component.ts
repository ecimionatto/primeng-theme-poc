import { Component } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';

import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  items: MenuItem[] = [
    {
      label: 'PrimeNG Components',
      items: [
        {
          label: 'Timeline',
          icon: 'pi pi-download',
          routerLink: '/timeline',
        },
        {
          label: 'Table',
          icon: 'pi pi-download',
          routerLink: '/table',
        },
        {
          label: 'Buttons with Progress',
          icon: 'pi pi-download',
          routerLink: '/buttons',
        },
        {
          label: 'Inputs',
          icon: 'pi pi-download',
          routerLink: '/inputs',
        },
        {
          label: 'Scroller',
          icon: 'pi pi-download',
          routerLink: '/scroller',
        },
        {
          label: 'SearchPanel',
          icon: 'pi pi-download',
          routerLink: '/searchPanel',
        },
      ],
    },
  ];

  constructor(private primengConfig: PrimeNGConfig) {}

  ngOnInit() {
    this.primengConfig.ripple = true;
  }
  title = 'primeng-poc';
}
