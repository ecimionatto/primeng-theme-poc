import { Component, OnInit } from '@angular/core';

import { Car } from './car';
@Component({
  selector: 'app-scroller',
  templateUrl: './scroller.component.html',
  styleUrls: ['./scroller.component.css'],
})
export class ScrollerComponent implements OnInit {
  constructor() {}

  cars: Array<Car> = [
    new Car('vin1', '2015', 'tesla', 'red'),
    new Car('vin2', '2010', 'honda', 'white'),
    new Car('vin3', '2019', 'toyota', 'greem'),
    new Car('vin1', '2015', 'tesla', 'red'),
    new Car('vin2', '2010', 'honda', 'white'),
    new Car('vin3', '2019', 'toyota', 'greem'),
    new Car('vin1', '2015', 'tesla', 'red'),
    new Car('vin2', '2010', 'honda', 'white'),
    new Car('vin3', '2019', 'toyota', 'greem'),
    new Car('vin1', '2015', 'tesla', 'red'),
    new Car('vin2', '2010', 'honda', 'white'),
    new Car('vin3', '2019', 'toyota', 'greem'),
    new Car('vin1', '2015', 'tesla', 'red'),
    new Car('vin2', '2010', 'honda', 'white'),
    new Car('vin3', '2019', 'toyota', 'greem'),
    new Car('vin1', '2015', 'tesla', 'red'),
    new Car('vin2', '2010', 'honda', 'white'),
    new Car('vin3', '2019', 'toyota', 'greem'),
    new Car('vin1', '2015', 'tesla', 'red'),
    new Car('vin2', '2010', 'honda', 'white'),
    new Car('vin3', '2019', 'toyota', 'greem'),
    new Car('vin1', '2015', 'tesla', 'red'),
    new Car('vin2', '2010', 'honda', 'white'),
    new Car('vin3', '2019', 'toyota', 'greem'),
    new Car('vin1', '2015', 'tesla', 'red'),
    new Car('vin2', '2010', 'honda', 'white'),
    new Car('vin3', '2019', 'toyota', 'greem'),
    new Car('vin1', '2015', 'tesla', 'red'),
    new Car('vin2', '2010', 'honda', 'white'),
    new Car('vin3', '2019', 'toyota', 'greem'),
    new Car('vin1', '2015', 'tesla', 'red'),
    new Car('vin2', '2010', 'honda', 'white'),
    new Car('vin3', '2019', 'toyota', 'greem'),
    new Car('vin1', '2015', 'tesla', 'red'),
    new Car('vin2', '2010', 'honda', 'white'),
    new Car('vin3', '2019', 'toyota', 'greem'),
  ];

  ngOnInit(): void {}
}
