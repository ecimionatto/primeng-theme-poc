import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TimelineComponent } from './timeline/timeline.component';
import { TableComponent } from './table/table.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { InputsComponent } from './inputs/inputs.component';
import { ScrollerComponent } from './scroller/scroller.component';
import { SearchPanelComponent } from './search-panel/search-panel.component';

const routes: Routes = [
  { path: 'timeline', component: TimelineComponent },
  { path: 'table', component: TableComponent },
  { path: 'buttons', component: ButtonsComponent },
  { path: 'inputs', component: InputsComponent },
  { path: 'scroller', component: ScrollerComponent },
  { path: 'searchPanel', component: SearchPanelComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
