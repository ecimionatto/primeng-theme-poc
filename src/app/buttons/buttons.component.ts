import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.css'],
})
export class ButtonsComponent implements OnInit {
  constructor() {}

  value: number = 0;
  showProgress: boolean = false;

  ngOnInit(): void {}

  handleClick(event: any) {
    var _this = this;
    this.showProgress = true;

    (function theLoop(i: number) {
      setTimeout(() => {
        if (--i) {
          theLoop(i);
        }
        _this.value++;
        if (_this.value >= 100) {
          _this.showProgress = false;
          _this.value = 0;
        }
      }, 50);
    })(100);
  }
}
